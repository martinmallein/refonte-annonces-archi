# POC : Refonte des annonces

Ce projet est un POC afin d'étudier une architecture qui permettrait de refondre la gestion des filtres et l'affichage des pages annonces et recherche du site public **safti.fr**.<br><br>
L'idée générale est:

- De pouvoir unifier le comportement actuel des pages _/recherche_ et _/annonces_ en un seul comportement
- D'utiliser l'URL comme state général pour tous nos filtres
- De modifier notre store lorsque l'URL est modifiée
- De récupérer nos nouveaux bien lorsque le store est modifié

![Schema image in src/assets/schema.png](https://gitlab.com/martinmallein/refonte-annonces-archi/-/raw/main/src/assets/schema.png)

## SEO

Actuellement les pages /recherche ne sont pas indexés pour les moteur de recherche. En unifiant les pages _/recherche_ dans _/annonces_ il faudrait desindexer les pages où les paramètres dynamiques [propertyType] ou [cityType] contiennent plusieurs valeurs.<br><br>
Exemple:

- ✅ https://safti.fr/annonces/achat/maison/toulouse-31500
- ✅ https://safti.fr/annonces/vente/appartment/marseille-all-cp?bedroom=4&budgetMax=420000&surfaceMin=84
- ❌ https://safti.fr/annonces/vente/appartment-maison/gironde-33_marseille-all-cp

## Installation & Démarrage

Créer un fichier `.env.local` à partir du fichier `.env.example` et remplir les deux variables d'environnements (les mêmes que pour le site public)

Installer les dépendances

```bash
npm i
```

Démarrer le serveur de développement:

```bash
npm run dev
```
