import Image from "next/image";
import Link from "next/link";

export default function Home() {
  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      <Link
        href={"/annonces/achat/maison/gironde-33"}
        className="bg-black rounded shadow p-4 text-white font-bold"
      >
        Go to /annonce page
      </Link>
    </main>
  );
}
