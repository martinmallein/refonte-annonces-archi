"use client";

import { AnnoncesDisplayer } from "@/components/AnnoncesDisplayer";
import { DataDisplayer } from "@/components/DataDisplayer";
import { Filters } from "@/components/Filters";
import { useUpdateStoreFromUrl } from "@/hooks/useUpdateStoreFromUrl";

export default function AnnoncesPage() {
  useUpdateStoreFromUrl();

  return (
    <div className="min-h-screen flex flex-col gap-16 max-w-7xl mx-auto py-16">
      <Filters />
      <DataDisplayer />
      <hr className="bg-gray-400 h-[2px] w-full" />
      <AnnoncesDisplayer />
    </div>
  );
}
