import { create } from "zustand";

interface AnnonceState {
  cityType?: string;
  sort: string | null;
  setKey: (key: keyof AnnonceState, value: string | undefined | null) => void;
}

export const useAnnonceStore = create<AnnonceState>()((set) => ({
  cityType: undefined,
  sort: null,
  setKey: (key, value) =>
    set((state) => {
      // Si on essaye de mettre à jour une valeur qui n'a pas changé on retourne le state actuel
      if (state[key] === value) return state;
      return { [key]: value };
    }),
}));
