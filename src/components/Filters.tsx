import { CityFilter } from "./CityFilter";
import Sort from "./Sort";

/**
 * Ce composant représente les filtres permettant de modifier notre URL
 *
 * @returns {JSX.Element}
 */
export function Filters() {
  return (
    <div className="flex gap-4">
      <CityFilter />
      <Sort />
    </div>
  );
}
