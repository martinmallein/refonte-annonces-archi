import { Option } from "@/types/Option";
import { useParams, useRouter, useSearchParams } from "next/navigation";
import { useEffect, useState } from "react";
import Select from "react-select";

const citiesOptions = [
  { value: "toulouse-31500", label: "Toulouse (31500)" },
  { value: "marseille-all-cp", label: "Marseille (tous les codes postaux)" },
  { value: "paris-75001", label: "Paris (75001)" },
  { value: "gironde-33", label: "Gironde (33)" },
];

/**
 * Ce composant permet de modifier le paramètre cityType de notre URL dynamique
 *
 * @returns {JSX.Element}
 */
export function CityFilter() {
  const [selectedCities, setSelectedCities] = useState<Option[]>();
  const params = useParams();
  const router = useRouter();
  const searchParams = useSearchParams();
  const { transactionType, propertyType, cityType } = params;

  const handleCityFilterChange = (options: any) => {
    setSelectedCities(options);
    const values = options.map((option: Option) => option.value);
    const newCityValue = values.join("_");
    if (newCityValue !== cityType && options.length > 0) {
      // Pas besoin de conditionner la présence du "?" si les searchParams sont vides, le router gère ce cas si nécessaire
      router.push(
        `/annonces/${transactionType}/${propertyType}/${newCityValue}?${searchParams.toString()}`
      );
    }
  };

  // On initialise notre filtre local en fonction de l'URL
  useEffect(() => {
    if (typeof cityType === "string") {
      const cityValues = cityType.split("_");
      const selectedOptions = citiesOptions.filter((city) =>
        cityValues.includes(city.value)
      );
      setSelectedCities(selectedOptions);
    }
  }, []);

  if (!selectedCities)
    return <div className="bg-gray-400 animate-pulse h-9 w-36 rounded" />;

  return (
    <Select
      options={citiesOptions}
      value={selectedCities}
      isMulti
      onChange={handleCityFilterChange}
      className="min-w-48"
      placeholder="Localisation"
    />
  );
}
