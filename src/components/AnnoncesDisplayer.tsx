import { useGetProperties } from "@/hooks/useGetProperties";

/**
 * Permet d'afficher les annonces
 *
 * @returns {JSX.Element}
 */
export function AnnoncesDisplayer() {
  const { data, isLoading } = useGetProperties();

  return (
    <div className="space-y-4">
      <h1 className="text-2xl font-bold">Annonces</h1>
      {isLoading ? (
        <p>Loading...</p>
      ) : (
        <ul className="grid grid-cols-4 gap-8">
          {data?.properties?.map((property: any) => (
            <li
              key={property.propertyReference}
              className="border rounded shadow"
            >
              <img
                src={property.photos[0].urlPhotoLarge}
                alt={property.propertyReference}
                className="w-full h-48 object-cover rounded-t"
              />
              <h3 className="p-4 font-semibold">{property.catchphrase}</h3>
            </li>
          ))}
        </ul>
      )}
    </div>
  );
}
