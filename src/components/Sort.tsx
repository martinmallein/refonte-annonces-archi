"use client";

import { Option } from "@/types/Option";
import { useSearchParams, useRouter } from "next/navigation";
import { useEffect, useState } from "react";
import Select from "react-select";

const sortOptions = [
  { value: "price_asc", label: "Budget croissant" },
  { value: "price_desc", label: "Budget décroissant" },
  { value: "time_asc", label: "Plus récentes" },
  { value: "time_desc", label: "Plus anciennes" },
];

/**
 * Ce composant permet de modifier le paramètre sort de notre query
 *
 * @returns {JSX.Element}
 */
export default function Sort() {
  const [selectedSort, setSelectedSort] = useState<Option>();
  const searchParams = useSearchParams();
  const router = useRouter();

  const handleSortUpdate = (option: any) => {
    const params = new URLSearchParams(searchParams.toString());
    params.set("sort", option.value);
    // Pas besoin de préciser le pathname, on ne touche qu'à nos searchParams
    router.push(`?${params.toString()}`);
  };

  // On initialise notre filtre local en fonction de l'URL
  useEffect(() => {
    const sort = searchParams.get("sort");
    setSelectedSort(sortOptions.find((option) => option.value === sort));
  }, []);

  return (
    <Select
      options={sortOptions}
      onChange={handleSortUpdate}
      value={selectedSort}
      placeholder="Sort by"
      className="min-w-48"
    />
  );
}
