import { useAnnonceStore } from "@/store/annoncesStore";
import { useParams, useSearchParams } from "next/navigation";

/**
 * Ce composant permet d'afficher les valeur de "params", "searchParams" et du store
 *
 * @returns {JSX.Element}
 */
export function DataDisplayer() {
  const params = useParams();
  const searchParams = useSearchParams();
  const { sort, cityType } = useAnnonceStore();

  return (
    <section className="flex justify-between gap-8">
      <ul className="space-y-2">
        <h2 className="text-xl font-semibold">Parameters from useParams</h2>
        {Object.entries(params).map((param, index) => (
          <li key={index}>
            {param[0]} = {param[1]}
          </li>
        ))}
      </ul>
      <ul className="space-y-2">
        <h2 className="text-xl font-semibold">
          searchParams from useSearchParams
        </h2>
        {Array.from(searchParams).map((param, index) => (
          <li key={index}>
            {param[0]} = {param[1]}
          </li>
        ))}
      </ul>
      <div className="space-y-2">
        <h2 className="text-xl font-semibold">Values from store</h2>
        <div className="space-y-2">
          <p>cityType = {cityType}</p>
          <p>sort = {sort}</p>
        </div>
      </div>
    </section>
  );
}
