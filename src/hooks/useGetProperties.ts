import { useAnnonceStore } from "@/store/annoncesStore";
import { useQuery } from "@tanstack/react-query";

/**
 * Ce hook permet de récupérer les propriétés quand les paramètres de recherche changent (pour l'exemple la query est invalidée quand cityType change)
 *
 * @returns {Object}
 */
export function useGetProperties() {
  const { cityType } = useAnnonceStore();

  // Le traitement du body en fonction des paramètres d'URL n'est pas traité il est en dur pour l'exemple
  const body = JSON.stringify({
    limit: 24,
    page: 1,
    propertyType: ["maison"],
    transactionType: "vente",
    locations: ["city-13186"],
  });

  console.log("Here with:", body);

  const { data, isLoading } = useQuery({
    queryKey: ["properties", cityType],
    queryFn: async () => {
      const response = await fetch(
        `${process.env.NEXT_PUBLIC_API_URL}/public_site/property/search`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${process.env.NEXT_PUBLIC_TOKEN_API}`,
          },
          body,
        }
      );
      return response.json();
    },
    enabled: !!cityType,
  });
  return { data, isLoading };
}
