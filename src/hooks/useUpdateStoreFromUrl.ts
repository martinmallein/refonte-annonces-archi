import { useAnnonceStore } from "@/store/annoncesStore";
import { useParams, usePathname, useSearchParams } from "next/navigation";
import { useEffect } from "react";

/**
 * Ce hook permet de mettre à jour le store zustand lorsque notre URL change
 */
export function useUpdateStoreFromUrl() {
  const pathname = usePathname();
  const params = useParams();
  const searchParams = useSearchParams();
  const { setKey } = useAnnonceStore();

  // Lorsqu'un des paramètre dynamique de l'URL change, on met à jour notre store
  useEffect(() => {
    const { cityType } = params;
    if (typeof cityType === "string") setKey("cityType", cityType);
  }, [pathname]);

  // Lorsqu'un des paramètre de la query change, on met à jour le store
  useEffect(() => {
    const sort = searchParams?.get("sort");
    setKey("sort", sort);
  }, [searchParams]);
}
